package test;

import application.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    /**
     * Test isInCapsule
     */
    @Test
    public void isInCapsule() {
        assertTrue(Main.isInCapsule(1, 2, 5, 2, 3.2f, 4, 2));
        assertTrue(Main.isInCapsule(1, 2, 5, 2, 3.2f, 6, 2));
        assertTrue(Main.isInCapsule(1, 2, 5, 2, 3.2f, 0, 3));
        assertTrue(Main.isInCapsule(1, 2, 5, 2, 3.2f, 4, 0));
        assertTrue(Main.isInCapsule(1, 3, 5, 1, 3.2f, 0, 3));
        assertTrue(Main.isInCapsule(1, 3, 5, 1, 3.2f, 1, 1));
        assertTrue(Main.isInCapsule(1, 3, 5, 1, 3.2f, 4, 5));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 2, 3, 2));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 10, 2, 2));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 0, 2, 5));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 2.82842f, 2, 2));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 2.82842f, 2, 2));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 2.82842f, 2, 6));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 2.82842f, 0, 1));
        assertTrue(Main.isInCapsule(2, 3, 2, 5, 2.82843f, 0, 1));
        //limits
        assertTrue(Main.isInCapsule(2e8f, 3e8f, 2e8f, 5e8f, 2.82843e8f, 0, 1e8f));
        assertTrue(Main.isInCapsule(2e-8f, 3e-8f, 2e-8f, 5e-8f, 2.82843e-8f, 0, 1e-8f));
        //precision
        assertTrue(Main.isInCapsule(2e-5f, 3e-5f, 2e-5f, 5e-5f, 2e-5f, 0, 1e-5f));
        assertTrue(Main.isInCapsule(2e-5f, 3e-5f, 2e-5f, 5e-5f, 2e-4f, 0, 1e-5f));
        assertTrue(Main.isInCapsule(2e-8f, 3e-8f, 2e-8f, 5e-8f, 1, 0, 1e-8f));
    }

    @Test
    public void isNotInCapsule() {
        assertFalse(Main.isInCapsule(2, 3, 2, 5, 2, 5, 2));
        assertFalse(Main.isInCapsule(2, 3, 2, 5, 2.82842f, 2, 8));
        assertFalse(Main.isInCapsule(2, 3, 2, 5, 2.82842f, 2, 0));
        assertFalse(Main.isInCapsule(2, 3, 2, 5, 2.82841f, 0, 1));
        //limits
        assertFalse(Main.isInCapsule(2e8f, 3e8f, 2e8f, 5e8f, 2.82842e8f, 0, 1e8f));
    }

    /**
     * Test calculating distance
     */
    @Test
    public void calculateDistance() {
        assertEquals(3.13049, Main.pointToSegmentDistance(new Point(1, 3), new Point(5, 1), new Point(4, 5)), Main.EPSILON);
        assertEquals(0.0, Main.pointToSegmentDistance(new Point(0, 0), new Point(0, 0), new Point(0, 0)), Main.EPSILON);
        assertEquals(0.0, Main.pointToSegmentDistance(new Point(1, 2), new Point(1, 6), new Point(1, 4)), Main.EPSILON);
        assertEquals(2.0, Main.pointToSegmentDistance(new Point(2, 3), new Point(2, 5), new Point(0, 4)), Main.EPSILON);
        assertEquals(2.82842, Main.pointToSegmentDistance(new Point(2, 3), new Point(2, 5), new Point(0, 1)), Main.EPSILON);
        assertEquals(2.82842, Main.pointToSegmentDistance(new Point(2, 3), new Point(2, 5), new Point(0, 7)), Main.EPSILON);
        assertEquals(1.0, Main.pointToSegmentDistance(new Point(2, 3), new Point(2, 5), new Point(2, 2)), Main.EPSILON);

        //precision test
        assertEquals(2e-5f, Main.pointToSegmentDistance(new Point(2e-5f, 3e-5f), new Point(2e-5f, 5e-5f), new Point(0, 1e-5f)), Main.EPSILON);
        assertEquals(0, Main.pointToSegmentDistance(new Point(2e-8f, 3e-8f), new Point(2e-8f, 5e-8f), new Point(0, 1e-8f)), Main.EPSILON);
    }

    /**
     * Test operations with vectors
     */
    final Vector v1 = new Vector(1e-8f, 1e-8f);
    final Vector v2 = new Vector(1e8f, 1e8f);
    final Vector v3 = new Vector(0, 0);
    final Vector v4 = new Vector(4, 2);
    final Vector v5 = new Vector(1, 1);

    @Test
    public void scalarProduct() {
        assertEquals(2e-16f, Main.scalarProduct(v1, v1), Main.EPSILON);
        assertEquals(2.0000000545128448E16, Main.scalarProduct(v2, v2), Main.EPSILON);
        assertEquals(0, Main.scalarProduct(v3, v4), Main.EPSILON);
        assertEquals(6, Main.scalarProduct(v4, v5), Main.EPSILON);
    }

    @Test
    public void pseudoScalarProduct() {
        assertEquals(0, Main.crossProduct(v1, v1), Main.EPSILON);
        assertEquals(0, Main.crossProduct(v2, v2), Main.EPSILON);
        assertEquals(0, Main.crossProduct(v3, v4), Main.EPSILON);
        assertEquals(2.0, Main.crossProduct(v4, v5), Main.EPSILON);
        assertEquals(2.0, Main.crossProduct(v5, v4), Main.EPSILON);
        assertEquals(0, Main.crossProduct(v1, v2), Main.EPSILON);
    }
}
