package test;

import application.*;
import org.junit.Test;

import static org.junit.Assert.*;

class VectorTest {

    final Vector v1 = new Vector(1e-8f, 1e-8f);
    final Vector v2 = new Vector(1e8f, 1e8f);
    final Vector v3 = new Vector(0, 0);
    final Vector v4 = new Vector(4, 2);
    @Test
    public void length() {
        assertEquals(1e-16f, v1.length(), 0.00001);
        assertEquals(1e16f, v2.length(), 0.00001);
        assertEquals(0, v3.length(), 0.00001);
        assertEquals(1e8, v4.length(), 0.00001);
    }

}