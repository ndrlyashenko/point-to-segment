package application;

import java.util.Scanner;

public class Main {
    /**
     * Calculation precision.
     */
    public static final float EPSILON = 1e-5f;
    /**
     * Point A(x1, y1) and point B(x2, y2) forms segment AB.
     * Let S be a set of points, that lies on the distance, not greater than d, from AB.
     *
     * @return true if point C(x3, y3) is an element of S.
     */
    public static boolean isInCapsule(float x1, float y1, float x2, float y2, float d, float x3, float y3) {
        final Point A = new Point(x1, y1);
        final Point B = new Point(x2, y2);
        final Point C = new Point(x3, y3);

        final float distance = pointToSegmentDistance(A, B, C);

        return distance - d <= EPSILON;
    }

    /**
     * Drop a perpendicular from C on AB.
     * If (AB, AC) < 0 or (AB, BC) < 0, a perpendicular does not fall on AB,
     * distance would be min(|AC|, |BC|).
     * Otherwise, perpendicular length is calculated with formula:
     *   h = |[AB, AC] / |AB||.
     * h is the aim distance.
     *
     * @return the distance between point C and segment AB.
     */
    public static float pointToSegmentDistance(Point A, Point B, Point C) {
        final Vector AB = new Vector(A, B);
        final Vector BA = new Vector(B, A);
        final Vector AC = new Vector(A, C);
        final Vector BC = new Vector(B, C);

        final float s1 = scalarProduct(AB, AC);
        final float s2 = scalarProduct(BA, BC);
        if(s1 < 0 || s2 < 0) {
            return Math.min(AC.length(), BC.length());
        }

        final float area = crossProduct(AB, AC);
        final float base = AB.length();
        if(base == 0) {
            return 0;
        }
        final float h = area / base;
        return h;
    }

    /**
     * Calculate scalar product:
     *   x1*x2 + y1*y2
     *
     * @return scalar product (vector1, vector2)
     */
    public static float scalarProduct(Vector vector1, Vector vector2) {
        return vector1.getX()*vector2.getX() + vector1.getY()*vector2.getY();
    }

    /**
     * Calculate cross product as:
     *   x1*y2 - y1*x2
     *
     * @return cross product |vector1, vector2|
     */
    public static float crossProduct(Vector vector1, Vector vector2) {
        return Math.abs(vector1.getX()*vector2.getY() - vector1.getY()*vector2.getX());
    }

    /**
     * Main method.
     */
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter x1, y1, x2, y2, d, x3, y3");
        float x1 = reader.nextFloat();
        float y1 = reader.nextFloat();
        float x2 = reader.nextFloat();
        float y2 = reader.nextFloat();
        float d = reader.nextFloat();
        float x3 = reader.nextFloat();
        float y3 = reader.nextFloat();

        System.out.println(isInCapsule(x1, y1, x2, y2, d, x3, y3));
    }
}
