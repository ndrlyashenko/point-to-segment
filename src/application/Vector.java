package application;

public class Vector {

    private final float x;
    private final float y;

    public Vector(Point point1, Point point2) {
        this.x = point2.getX() - point1.getX();
        this.y = point2.getY() - point1.getY();
    }
    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float length() {
        return (float)Math.sqrt(Math.pow((double) this.getX(), 2) + Math.pow((double) this.getY(), 2));
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
